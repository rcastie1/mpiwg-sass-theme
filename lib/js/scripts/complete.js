var documentHeight, windowHeight;
var scrollOffset = 100;
var lastScrollPosition = 0;
var stickyMenuFn = debounce(function() {
    
    var scrollPos = $(window).scrollTop();
    var stickyMenu = $('.nav_accordeon.sticky').first();

    if(scrollPos > scrollOffset && scrollPos < documentHeight - windowHeight - scrollOffset) {
        if(scrollPos < lastScrollPosition) {
            stickyMenu.removeClass('sticky_hiden');
        } else {
            stickyMenu.addClass('sticky_hiden');
        }
    } else {
        stickyMenu.removeClass('sticky_hiden');
    }
    //
    if(stickyMenu.hasClass('sticky_hiden') && stickyMenu.find('.nav_accordion_check:checked').length > 0) {
        stickyMenu.find('label.hamburger_label').first().trigger('click');
    }
    
    lastScrollPosition = scrollPos;

}, 100, false);


$(document).ready(function() {
    documentHeight = getDocScrollHeight();
    windowHeight = getWindowHeight();
});


$(window).on('scroll', function() {
    stickyMenuFn();
});
$(window).on('resize', function() {
    documentHeight = getDocScrollHeight();
    windowHeight = getWindowHeight();

});
    
/**
 * Returns a html document scroll height
 */
function getDocScrollHeight() {
    var body = document.body,
        html = document.documentElement;

    var height = Math.max( body.scrollHeight, body.offsetHeight, 
                           html.clientHeight, html.scrollHeight, html.offsetHeight );
    return height;
}

/**
 * Returns a window height
 */
function getWindowHeight() {
    return $(window).height();
}


/**
 * Returns a function, that, as long as it continues to be invoked, will not
 * be triggered. The function will be called after it stops being called for
 * N milliseconds. If `immediate` is passed, trigger the function on the
 * leading edge, instead of the trailing.
 * 
 * @param function func
 * @param integer wait
 * @param boolean immediate
 */
function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};

