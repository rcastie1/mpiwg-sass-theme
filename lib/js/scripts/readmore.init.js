(function ($) {
  Drupal.behaviors.readMore = {
    attach: function (context, settings) {
      $('.field--name-field-text').once().each(function() {
        var that = this;
        if ($('.read_more_part', that).length <= 1) {
          return;
        }
        var $more = $('<button>', {
          class: 'button bg-dunkelgruen color-neongruen',
          text: 'Read More'
        });
        $(that).append($more);
        $more.wrap('<div class="text-center readmore"></div>');
        $more.click(function(){
          $('.read_more_part:visible:last', that).next('.read_more_part').show();
          if ($('.read_more_part:hidden', that).length <= 0) {
            $more.remove();
          }
        });
      })
    }
  }
})(jQuery)