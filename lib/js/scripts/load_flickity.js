var Flickity = require('flickity');

(function ($) {
  $('.lightbox-carousel-imgs').each(function(obj) {
    var flkty = new Flickity(this,{
      wrapAround: false,
      imagesLoaded: true
    });
});

})(jQuery);
