# How to use (webpack)

```
$fonts : '../node_modules/mpiwg_sass_theme/lib/fonts';
@import '~mpiwg_sass_theme/lib/scss/vars';
@include 'custom_vars';
@import '~mpiwg_sass_theme/lib/scss/mpiwg';
```

## Installation


```
npm i git+ssh://git@gitlab.gwdg.de:b.connect/mpiwg-sass-theme.git --save
```
